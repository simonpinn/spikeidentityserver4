﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4.Validation;

namespace Spike.IdentityServer4.Server.Services
{
    public class CustomResourceOwnerPasswordValidator: IResourceOwnerPasswordValidator
    {
        private readonly IUserService _userService;

        public CustomResourceOwnerPasswordValidator(IUserService userService)
        {
            _userService = userService;
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            if (await _userService.ValidateCredentials(context.UserName, context.Password))
            {
                var user = await _userService.FindByUsername(context.UserName);
                context.Result = new GrantValidationResult(user.SubjectId, OidcConstants.AuthenticationMethods.Password);
            }
        }
    }
}
