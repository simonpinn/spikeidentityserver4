﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Stores;

namespace Spike.IdentityServer4.Server.Services
{
    public class ResourceService : IResourceStore
    {
        private readonly List<IdentityResource> _identityResources = new List<IdentityResource> {
            new IdentityResources.OpenId(),
            new IdentityResources.Profile(),
            new IdentityResources.Email(),
            new IdentityResource {
                Name = "role",
                UserClaims = new List<string> {"role"}
            }
        };

        private readonly List<ApiResource> _apiResources = new List<ApiResource> {
            new ApiResource {
                Name = "customAPI",
                DisplayName = "Custom API",
                Description = "Custom API Access",
                UserClaims = new List<string> {"role"},
                ApiSecrets = new List<Secret> {new Secret("scopeSecret".Sha256())},
                Scopes = new List<Scope> {
                    new Scope("customAPI.read"),
                    new Scope("customAPI.write")
                }
            }
        };

        public Task<IEnumerable<IdentityResource>> FindIdentityResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            return Task.FromResult(_identityResources.Where(x => scopeNames.Contains(x.Name)));
        }

        public Task<IEnumerable<ApiResource>> FindApiResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            return Task.FromResult(_apiResources.Where(x => scopeNames.Contains(x.Name)));
        }

        public Task<ApiResource> FindApiResourceAsync(string name)
        {
            return Task.FromResult(_apiResources.SingleOrDefault(x => x.Name == name));
        }

        public Task<Resources> GetAllResourcesAsync()
        {
            return Task.FromResult(new Resources(_identityResources, _apiResources));
        }
    }
}
