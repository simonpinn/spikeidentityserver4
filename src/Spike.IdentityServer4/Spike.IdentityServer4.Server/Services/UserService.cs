﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spike.IdentityServer4.Server.Services
{
    public interface IUserService
    {
        Task<bool> ValidateCredentials(string username, string password);

        Task<CustomUser> FindBySubjectId(string subjectId);

        Task<CustomUser> FindByUsername(string username);
        Task<CustomUser> FindByExternalProvider(string provider, string providerId);
    }

    public class UserService : IUserService
    {
        private readonly List<CustomUser> _users = new List<CustomUser>
        {
            new CustomUser
            {
                UserName = "simon@spinn.net.au",
                Email = "simon@spinn.net.au",
                Password = "password",
                SubjectId = "1",
            }
        };

        public Task<bool> ValidateCredentials(string username, string password)
        {
            return Task.FromResult(_users.Any(x => x.UserName == username && x.Password == password));
        }

        public Task<CustomUser> FindBySubjectId(string subjectId)
        {
            return Task.FromResult(_users.FirstOrDefault(x => x.SubjectId == subjectId));
        }

        public Task<CustomUser> FindByUsername(string username)
        {
            return Task.FromResult(_users.FirstOrDefault(x => x.UserName == username));
        }

        public Task<CustomUser> FindByExternalProvider(string provider, string providerId)
        {
            throw new NotImplementedException();
        }
    }


    public class CustomUser
    {
        public string SubjectId { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
