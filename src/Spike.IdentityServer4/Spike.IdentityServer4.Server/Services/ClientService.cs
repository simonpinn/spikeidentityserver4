﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Stores;

namespace Spike.IdentityServer4.Server.Services
{
    public class ClientService : IClientStore
    {
        private readonly List<Client> _clients = new List<Client>
        {
            new Client
            {
                ClientId = "oauthClient",
                ClientName = "Example Client Credentials Client Application",
                AllowedGrantTypes = GrantTypes.ClientCredentials,
                ClientSecrets = new List<Secret>
                {
                    new Secret("superSecretPassword".Sha256())
                },
                AllowedScopes = new List<string> {"customAPI.read"}
            },
            new Client
            {
                ClientId = "openIdConnectClient",
                ClientName = "Example OpenId CLient",
                AllowedGrantTypes = GrantTypes.Implicit,
                AllowedScopes = new List<string>
                {
                    "customAPI.read",
                    IdentityServerConstants.StandardScopes.OpenId,
                    IdentityServerConstants.StandardScopes.Profile
                },
                RedirectUris = new List<string> {"https://localhost:5002/signin-oidc"},
                RequireConsent = true,
            }
        };

        public Task<Client> FindClientByIdAsync(string clientId)
        {
            return Task.FromResult(_clients.SingleOrDefault(x => x.ClientId == clientId));
        }
    }
}
