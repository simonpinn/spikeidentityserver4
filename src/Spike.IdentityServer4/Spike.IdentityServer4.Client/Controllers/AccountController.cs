﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Spike.IdentityServer4.Client.Controllers
{
    public class AccountController : Controller
    {
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Login(string returnUrl)
        {
            base.Redirect(returnUrl);
            return Ok();
        }

    }
}
